import 'package:flutter/material.dart';

class SaveButton extends StatelessWidget {
  Function onPressed;

  SaveButton({Key key, @required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(0.0),
          side: BorderSide(color: Colors.greenAccent.shade400)),
      onPressed: () {
        onPressed();
      },
      color: Colors.greenAccent.shade400,
      textColor: Colors.white,
      child: Icon(Icons.save),
    );
  }
}
