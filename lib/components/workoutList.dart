import 'package:flutter/material.dart';
import 'package:test_flutter_project/models/workout.dart';
import 'package:test_flutter_project/widgets/subtitle.dart';

class WorkoutList extends StatefulWidget {
  @override
  _WorkoutListState createState() => _WorkoutListState();
}

class _WorkoutListState extends State<WorkoutList> {
  @override
  void initState() {
    clearFilter();
    super.initState();
  }

  bool filterSwitchWorkouts = false;
  String filterTitle = '';
  var filterTitleContoller = TextEditingController();
  String filterLevel = 'Any level';
  String filterText = '';
  double filterHeight = 0.0;

  var levelMenuItems = <String>[
    'Any level',
    'Beginner',
    'Intermediate',
    'Advanced'
    // ignore: top_level_function_literal_block
  ].map((String value) {
    return new DropdownMenuItem(
      value: value,
      child: new Text(value),
    );
  }).toList();

  List<Workout> filter() {
    setState(() {
      filterText = filterSwitchWorkouts ? 'MyWorkouts' : 'All workouts';
      filterText += '/' + filterLevel;
      if (filterTitle.isNotEmpty) {
        filterText += '/' + filterTitle;
      }
      filterHeight = 0;
    });
  }

  List<Workout> clearFilter() {
    setState(() {
      filterText = 'All workouts/Any level';
      filterSwitchWorkouts = false;
      filterTitle = '';
      filterLevel = 'Any level';
      filterTitleContoller.clear();
      filterHeight = 0;
    });
  }

  final workouts = <Workout>[
    Workout(
        title: 'Test workout1',
        author: 'Test authhor1',
        description: 'Test description1',
        level: 'beginner'),
    Workout(
        title: 'Test workout2',
        author: 'Test authhor2',
        description: 'Test description2',
        level: 'intermidiate'),
    Workout(
        title: 'Test workout3',
        author: 'Test authhor3',
        description: 'Test description3',
        level: 'advanced'),
    Workout(
        title: 'Test workout4',
        author: 'Test authhor4',
        description: 'Test description4',
        level: 'intermidiate'),
    Workout(
        title: 'Test workout5',
        author: 'Test authhor5',
        description: 'Test description5',
        level: 'advanced'),
  ];

  @override
  Widget build(BuildContext context) {
    var workoutslistView = ListView.builder(
        shrinkWrap: true,
        itemCount: workouts.length,
        itemBuilder: (context, i) {
          return Card(
            elevation: 2.0,
            child: Container(
              decoration: BoxDecoration(color: Colors.lightBlue),
              child: ListTile(
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  child: Icon(
                    Icons.analytics_outlined,
                    color: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(width: 1, color: Colors.white))),
                ),
                title: Text(workouts[i].title,
                    style: TextStyle(color: Colors.white)),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ),
                subtitle: subtitle(context, workouts[i]),
              ),
            ),
          );
        });
    var filterInfo = Container(
      margin: EdgeInsets.only(top: 13, left: 5, right: 5, bottom: 13),
      decoration: BoxDecoration(color: Colors.white.withOpacity(0.7)),
      height: 40,
      child: TextButton(
        onPressed: () {
          setState(() {
            filterHeight = (filterHeight == 0.0 ? 280.0 : 0.0);
          });
        },
        child: Row(
          children: [Icon(Icons.filter_alt_sharp), Text(filterText)],
        ),
      ),
    );
    var filterForm = AnimatedContainer(
      child: Card(
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  child: SwitchListTile(
                    title: Text('Only my workouts'),
                    value: filterSwitchWorkouts,
                    onChanged: (bool val) =>
                        setState(() => filterSwitchWorkouts = val),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(labelText: 'Level'),
                    items: levelMenuItems,
                    value: filterLevel,
                    onChanged: (String val) =>
                        setState(() => filterLevel = val),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: TextFormField(
                    controller: filterTitleContoller,
                    decoration: InputDecoration(labelText: 'Title'),
                    onChanged: (String val) =>
                        setState(() => filterTitle = val),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: TextButton(
                            onPressed: () {
                              filter();
                            },
                            child: Text(
                              'Apply',
                              style: TextStyle(color: Colors.lightBlueAccent),
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: TextButton(
                            onPressed: () {
                              clearFilter();
                            },
                            child: Text(
                              'Clear',
                              style: TextStyle(color: Colors.red),
                            )),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      duration: Duration(milliseconds: 500),
      curve: Curves.bounceIn,
      height: filterHeight,
    );

    return ListView(
      children: [
        Column(
          children: [
            filterInfo,
            filterForm,
            workoutslistView,
          ],
        )
      ],
    );
  }
}
