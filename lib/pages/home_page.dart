import 'package:flutter/material.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:test_flutter_project/components/activeWorkouts.dart';
import 'package:test_flutter_project/components/workoutList.dart';
import 'package:test_flutter_project/pages/add-workout.dart';
import 'package:test_flutter_project/services/authService.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  SnakeBarBehaviour snakeBarStyle = SnakeBarBehaviour.floating;
  SnakeShape snakeShape = SnakeShape.circle;
  Color selectedColor = Colors.lightBlueAccent;
  bool showSelectedLabels = false;
  bool showUnselectedLabels = false;
  int _selectedItemPosition = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[900],
      appBar: AppBar(
        title: Text(
          'Fitness App',
          style: TextStyle(color: Colors.white),
        ),
        leading: Icon(
          Icons.fitness_center,
          color: Colors.white,
        ),
        actions: [
          TextButton(
              onPressed: () {
                AuthService().logOut();
              },
              child: Icon(
                Icons.exit_to_app,
                color: Colors.white,
              ))
        ],
      ),
      body: Container(
        child: _selectedItemPosition == 0 ? ActiveWorkouts() : WorkoutList(),
      ),
      bottomNavigationBar: SnakeNavigationBar.color(
        behaviour: snakeBarStyle,
        snakeShape: snakeShape,

        ///configuration for SnakeNavigationBar.color
        snakeViewColor: selectedColor,
        selectedItemColor:
            snakeShape == SnakeShape.indicator ? selectedColor : null,
        unselectedItemColor: Colors.blueGrey,

        ///configuration for SnakeNavigationBar.gradient
        //snakeViewGradient: selectedGradient,
        //selectedItemGradient: snakeShape == SnakeShape.indicator ? selectedGradient : null,
        //unselectedItemGradient: unselectedGradient,

        showUnselectedLabels: showUnselectedLabels,
        showSelectedLabels: showSelectedLabels,

        currentIndex: _selectedItemPosition,
        onTap: (index) => setState(() => _selectedItemPosition = index),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.fitness_center), label: 'My workouts'),
          BottomNavigationBarItem(
              icon: Icon(Icons.search), label: 'Find workouts'),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.white,
        foregroundColor: Colors.blue[900],
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (ctx) => AddWorkout()));
        },
      ),
    );
  }
}
