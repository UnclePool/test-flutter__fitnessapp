import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_flutter_project/pages/auth.dart';
import 'package:test_flutter_project/pages/home_page.dart';
import 'package:test_flutter_project/models/user.dart';

class LandingPage extends StatelessWidget {
  const LandingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appUser user = Provider.of<appUser>(context);
    final bool isLoggedIn = user != null;

    return isLoggedIn ? HomePage() : AuthPage();
  }
}
