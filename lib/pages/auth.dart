import 'package:flutter/material.dart';
import 'package:test_flutter_project/services/authService.dart';
import 'package:test_flutter_project/models/user.dart';
import 'package:toast/toast.dart';

class AuthPage extends StatefulWidget {
  AuthPage({Key key}) : super(key: key);

  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  String _email;
  String _password;
  bool isShowLogin = true;

  AuthService _authService = AuthService();

  @override
  Widget build(BuildContext context) {
    Widget _logo() {
      return Container(
        child: Text(
          'FITNESS APP',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 35.0),
        ),
      );
    }

    Widget _input(Icon icon, String hint, TextEditingController controller,
        bool obscure) {
      return Container(
        child: TextField(
          controller: controller,
          obscureText: obscure,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              hintStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  color: Colors.white30),
              hintText: hint,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 3)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white54, width: 1)),
              prefixIcon: icon),
        ),
      );
    }

    Widget _button(String text, func()) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.lightBlue[700],
            onPrimary: Colors.white,
            shadowColor: Colors.white,
            elevation: 3,
            textStyle: TextStyle(fontSize: 20),
            side: BorderSide(color: Colors.white54, width: 2)),
        onPressed: () {
          func();
        },
        child: Text(text),
      );
    }

    Widget _form(String btnName, void func()) {
      return Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20.0),
              child:
                  _input(Icon(Icons.email), 'EMAIL', _emailController, false),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: _input(
                  Icon(Icons.lock), 'PASSWORD', _passwordController, true),
            ),
            SizedBox(
              height: 30.0,
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20.0),
              width: double.infinity,
              height: 50.0,
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: _button(btnName, func),
            ),
          ],
        ),
      );
    }

    void _signInButtonAction() async {
      _email = _emailController.text;
      _password = _passwordController.text;

      if (_email.isEmpty && _password.isEmpty) return;

      appUser user = await _authService.signInWithEmailAndPassword(
          _email.trim(), _password.trim());

      if (user == null) {
        Toast.show("Email or password incorrect. Try again!", context,
            duration: Toast.LENGTH_LONG,
            gravity: Toast.TOP,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      } else {
        _emailController.clear();
        _passwordController.clear();
      }
    }

    void _signUpButtonAction() async {
      _email = _emailController.text;
      _password = _passwordController.text;

      if (_email.isEmpty && _password.isEmpty) return;

      appUser user = await _authService.registerWithEmailAndPassword(
          _email.trim(), _password.trim());

      if (user == null) {
        Toast.show("Something went wrong. Try later!", context,
            duration: Toast.LENGTH_LONG,
            gravity: Toast.TOP,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      } else {
        _emailController.clear();
        _passwordController.clear();
      }
    }

    return Scaffold(
      backgroundColor: Colors.lightBlue[900],
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _logo(),
            (isShowLogin
                ? Column(
                    children: [
                      _form('SIGN IN', _signInButtonAction),
                      Container(
                        child: GestureDetector(
                          child: Text(
                            'No registered?',
                            style: TextStyle(color: Colors.white),
                          ),
                          onTap: () {
                            setState(() {
                              isShowLogin = false;
                            });
                          },
                        ),
                      )
                    ],
                  )
                : Column(
                    children: [
                      _form('SIGN UP', _signUpButtonAction),
                      Container(
                        child: GestureDetector(
                          child: Text(
                            'Registered?',
                            style: TextStyle(color: Colors.white),
                          ),
                          onTap: () {
                            setState(() {
                              isShowLogin = true;
                            });
                          },
                        ),
                      )
                    ],
                  )),
          ],
        ),
      ),
    );
  }
}
