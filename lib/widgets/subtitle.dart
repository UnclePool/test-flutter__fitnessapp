import 'package:flutter/material.dart';
import 'package:test_flutter_project/models/workout.dart';

Widget subtitle(BuildContext context, Workout workout) {
  var color;
  double indicatorLevel;

  switch (workout.level) {
    case 'beginner':
      color = Colors.green;
      indicatorLevel = 0.33;
      break;
    case 'intermidiate':
      color = Colors.yellow;
      indicatorLevel = 0.66;
      break;
    case 'advanced':
      color = Colors.red;
      indicatorLevel = 1;
      break;
  }

  return Row(
    children: [
      Expanded(
        flex: 1,
        child: LinearProgressIndicator(
          backgroundColor: Colors.white,
          value: indicatorLevel,
          valueColor: AlwaysStoppedAnimation(color),
        ),
      ),
      SizedBox(width: 10.0),
      Expanded(
        flex: 3,
        child: Text(
          workout.level,
          style: TextStyle(color: Colors.white),
        ),
      )
    ],
  );
}
