import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_flutter_project/pages/landing.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:test_flutter_project/services/authService.dart';
import 'package:test_flutter_project/models/user.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamProvider<appUser>.value(
      value: AuthService().currentUser,
      child: MaterialApp(
        title: 'Quiz App',
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
          fontFamily: 'Georgia',
        ),
        home: LandingPage(),
      ),
    );
  }
}
